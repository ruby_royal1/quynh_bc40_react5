import React, { Component } from 'react'
import { connect } from 'react-redux'
import { deleteAction } from './redux/action/ShoeAction'
import { shoeReducer } from './redux/Reducer/shoeReducer'

class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.soLuong}</td>
                    <td>
                        <button className='btn btn-danger' style={{ borderRadius: '50%' }}>-</button>
                        <span className='mx-3'>{item.soLuong}</span>
                        <button className='btn btn-success' style={{ borderRadius: '50%' }}>+</button>
                    </td>
                    <td><img src={item.image} alt="" style={{ width: 40 }} /></td>
                    <td><button
                        onClick={() => {
                            this.props.handleDelete(item.id)
                        }}
                        className='btn btn-danger'>Delete</button></td>
                </tr>
            )
        })


    }
    render() {
        return (

            <div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Img</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTbody()}
                    </tbody>
                </table>
            </div>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleDelete: (id) => {
            // let action = {
            //     type: DELETE_ITEM,
            //     payload: id,
            // }
            dispatch(deleteAction(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
