import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addToCartAction } from './redux/action/ShoeAction';

class ItemShoe extends Component {

    render() {
        console.log("item props", this.props);
        let { id, image, name, price, shortDescription } = this.props.item;
        return (
            <div className=' col-3 p-4'>
                <div className="card">
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <h6>$ {price}</h6>
                        <p className="card-text">{shortDescription}</p>
                        <a href="#" onClick={() => {
                            this.props.handlePushToCart(this.props.item)
                        }} className="btn btn-primary">Add to cart</a>
                    </div>
                </div>

            </div>
        )
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handlePushToCart: (shoe) => {
            // let action = {
            //     type: ADD_TO_CART,
            //     payload: shoe
            // }
            dispatch(addToCartAction(shoe));
        }
    }
}
export default connect(null, mapDispatchToProps)(ItemShoe)