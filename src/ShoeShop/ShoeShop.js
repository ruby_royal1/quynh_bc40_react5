import React, { Component } from 'react'
import Cart from './Cart';
import { dataShoe } from './data_shoe';
import ListShoe from './ListShoe'


export default class ShoeShop extends Component {
    state = {
        listShoe: dataShoe,
        cart: [],
    }
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart];

        let index = cloneCart.findIndex((item) => {
            return item.id === shoe.id
        })
        // TH1: Sản phẩm chưa có trong giỏ hàng thì push
        if (index == -1) {
            let newShoe = { ...shoe, soLuong: 1 }
            cloneCart.push(newShoe)
        } else {
            cloneCart[index].soLuong++;
        }

        // TH2: Sản phẩm đã có trong giỏ hàng thì update số lượng


        this.setState({
            cart: cloneCart
        })
    }

    render() {
        return (
            <div className='container'>
                <h3>Cyber hoe Shop</h3>
                <Cart />
                <ListShoe />
            </div>
        )
    }
}
