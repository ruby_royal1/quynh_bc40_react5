import React, { Component } from 'react'
import { dataShoe } from '../../data_shoe'
import * as types from '../constants/ShoeConstants'
let initialValue = {
    listShoe: dataShoe,
    cart: [],
}

export const shoeReducer = (state = initialValue, action) => {
    switch (action.type) {
        case types.ADD_TO_CART: {
            console.log("add", action);
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            })

            if (index == -1) {
                let newShoe = { ...action.payload, soLuong: 1 }
                cloneCart.push(newShoe)
            } else {
                cloneCart[index].soLuong++;
            }
            return { ...state, cart: cloneCart };
        };
        case types.DELETE_ITEM: {
            console.log("xoa het ne");
            let newCart = state.cart.filter((item) => {
                return item.id != action.payload;
            });
            return { ...state, cart: newCart }
        }
        default:
            return state
    }
}
