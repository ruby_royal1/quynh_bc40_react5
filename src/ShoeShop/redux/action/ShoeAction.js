import { ADD_TO_CART, DELETE_ITEM } from "../constants/ShoeConstants"

export const addToCartAction = (shoe) => {
    return {
        type: ADD_TO_CART,
        payload: shoe
    }
}

export const deleteAction = (id) => {
    return {
        type: DELETE_ITEM,
        payload: id,
    }
}